%% Thomas Errington
%% Assignment 4 - Erlang - CS3060 - 22Oct17


-module(runme).
-export([problemOne/0]).
-export([problemTwo/1]).
-export([problemThree/1]).
-export([problemFour/1]).
-export([problemFive/1]).
-export([problemSix/2]).
-export([problemSeven/2]).
-export([problemEight/1]).
-export([problemNine/1]).
-export([problemTen/3]).


%% ====================================================================
%% Internal functions
%% ====================================================================

%% ==============================1=====================================
%% prints hello world

problemOne() ->
	io:format("Hello, world.~n", []).

%% ==============================2=====================================
%% counts the number of chars in a a string

problemTwo([]) -> 0;
	problemTwo(String) -> [_ | Tail] = String,	
	1 + problemTwo(Tail).

%% ==============================3=====================================
%% counts the number of words in a string

problemThree(String) ->
	Word = re:split(String, " "),
	problemTwo(Word).

%% ==============================4=====================================
%% recursively counts to N

problemFour(Number) ->
	helper(1, Number).

helper(Acc, Number) when Acc =< Number ->
	io:format("~w~n", [Acc]),
	helper(Acc+1, Number).

%% ==============================5=====================================
%% Displays error message depending on input

problemFive(success) ->
	io:format("Success~n", []);
problemFive({error, Message}) ->
	io:format("~s~n", [Message]).

%% ==============================6=====================================
%% matches tuples based on a keyword

problemSix(Value, Pairs) ->
	case lists:keyfind(Value, 1, Pairs) of
		{Value, Phrase} -> Phrase;
		false -> nothing
	end.

%% ==============================7=====================================
%% matrix multiplication

problemSeven(Amt, Cap) -> 
	random:seed(),
	lists:map(
	  fun(Xaxis) ->
			  lists:map(
				fun(Yaxis) ->
						case Yaxis of
							Xaxis -> 0;
							_ -> random:uniform(Cap)
						end
				end,
				lists:seq(1, Amt))
	  end,
	  lists:seq(1, Amt)).

%% need to implement the multi portion still

%% ==============================8=====================================
%% fibonacci

problemEight(0) -> 0;
problemEight(1) -> 1;
problemEight(Fib) -> 
	problemEight(Fib - 1) + problemEight(Fib - 2).

%% ==============================9=====================================
%% creates three process chain and sends message M times

problemNine(0) ->
	receive
		_ -> ok
	after 2000 -> 
		exit ("Chain Dies")
	end;

problemNine(M) ->
	Pid = spawn(fun() -> problemNine(3 - 1) end),
	link(Pid),
	receive
		_ -> ok
		
	end.


%% ==============================10====================================
%% creates N size loop then sends M messages

problemTen(0) -> 
	receive 
		_ -> ok
	after 2000 -> 
		exit ("Chain Dies")
	end;

problemTen(Nodes, M, Message) ->
	Pid = spawn(fun() -> problemTen(Nodes - 1) end),
	link(Pid),
	receive
		_ -> ok
	end.
