Thomas Errington
CS3060
A4 - 22OCT17

IDE - Eclipse with erlide installed
RUN - Eclipse compiles everything at once then I would call functions via the built in terminal
COMMANDS - 
	runme:problemOne().
	runme:problemTwo("Test").
	runme:problemThree("This is a Test").
	runme:problemFour(10).
	runme:problemFive({error, Message}).	runme:problemFive(success).
	runme:problemSix(erlang, [{erlang, "a functional language"} , {ruby, "an OO language"}]).
	runme:problemSeven(50, 100).
	runme:problemEight(10).
	runme:problemNine(4).
	runme:problemTen(4, 3, "A thing").
	
	
	
Sample Output:
Eshell V9.1
(Errington_Erland_A4@thomape58)1> runme:problemOne().
Hello, world.
ok
(Errington_Erland_A4@thomape58)2> runme:problemTwo("Test").
4
(Errington_Erland_A4@thomape58)3> runme:problemThree("This is a test").
4
(Errington_Erland_A4@thomape58)4> runme:problemFour(25).
1
2
3
4
5
6
7
8
9
10
11
12
13
14
15
16
17
18
19
20
21
22
23
24
25
(Errington_Erland_A4@thomape58)5> runme:problemFive(success).
Success
ok
(Errington_Erland_A4@thomape58)6> runme:problemSix(erlang, [{erlang, "a functional language"} , {ruby, "an OO language"}]).
"a functional language"
(Errington_Erland_A4@thomape58)7> runme:problemSix(erlang, [{ruby, "a functional langauge"}, {ruby, "an OO Language"}]).
nothing
(Errington_Erland_A4@thomape58)8> runme:problemSix(ruby, [{erlang, "a functional langauge"}, {ruby, "an OO Language"}]).
"an OO Language"
(Errington_Erland_A4@thomape58)9> runme:problemSeven(20, 100).
** exception error: undefined function runme:problemSeven/2
(Errington_Erland_A4@thomape58)10> runme:problemEight(10).
55
(Errington_Erland_A4@thomape58)11> runme:problemNine(10).
** exception exit: "Chain Dies"

***End output***


NOTES - problem seven has the creation portion for a randomly filled matrix only, the actual multiplacation has not been completed
	  - problem nine and ten are partially complete